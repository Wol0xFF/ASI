package com.cpe.springboot.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.RequestContext;

import com.cpe.springboot.model.UserModel;
import com.cpe.springboot.repository.UserRepository;

@RestController
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/users/{userId}")
	public Page<UserModel> getUserById(@PathVariable (value = "userId") Long userId, Pageable pageable){
		return userRepository.findById(userId, pageable);
	}
	
	@PostMapping("/user")
	public UserModel createUser(@Valid @RequestBody UserModel user) {
		return userRepository.save(user);
	}

	@PostMapping("/user/auth")
	public boolean authenticate(@RequestBody String ident) {
		String[] split = ident.split("&");
		String username, password;
		username = split[0].replaceAll("=", "");
		password = split[1].replaceAll("=", "");
		UserModel user = this.userRepository.findByUsernameAndPassword(username, password);
		//request.getSession().setAttribute("user", user);
		return user != null;
	}
}
