package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class RestJpaMysqlSpringBootSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestJpaMysqlSpringBootSampleApplication.class, args);
	}
}
