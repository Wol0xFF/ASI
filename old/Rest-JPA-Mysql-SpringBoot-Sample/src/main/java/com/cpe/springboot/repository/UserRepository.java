package com.cpe.springboot.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.*;


import com.cpe.springboot.model.UserModel;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long>{
	Page<UserModel>findById(Long id,Pageable pageable);
	UserModel findByUsernameAndPassword(String username, String password);
}
