package com.cpe.springboot.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	private String username;
	private String password;
	
	/*@OneToMany(cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private Set<CardModel>cards = new HashSet<>();*/

	public UserModel() {
		super();
		this.name="";
		this.username="";
		this.password="";
	}

	public UserModel(String name, String username, String password) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/*public Set<CardModel> getCards() {
		return cards;
	}

	public void setCards(Set<CardModel> cards) {
		this.cards = cards;
	}*/
	
	
	
}
