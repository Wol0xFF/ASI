package com.cpe.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ViewController {
	
	@GetMapping("/login")
	public String login() {
		return "user/login";
	}
	
	@GetMapping("/addUser")
	public String addUser() {
		return "user/addUser";
	}
	
	@GetMapping("/card")
	public String card() {
		return "card/cardHome";
	}
	
	@GetMapping("/cardList")
	public String cardList() {
		return "card/cardList";
	}
	

}
