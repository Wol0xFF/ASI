package springboot.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import springboot.model.UserDto;



@RestController
public class FacadeController {
	
	private static String URL_USER_MG = "http://localhost:8081";
	
	@PostMapping("/api/user")
	public UserDto createUser(@RequestBody UserDto user ) {
		RestTemplate rt= new RestTemplate();
		String url = URL_USER_MG + "/user";
		UserDto res = rt.postForObject(url, user, UserDto.class );
		
		return res;
	}
	
	
	@GetMapping("/api/users/{userId}")
	public UserDto getUserById(@PathVariable (value = "userId") long userId){
		RestTemplate rt= new RestTemplate();
		String url = URL_USER_MG + "/users/" + userId;
		UserDto res = rt.getForObject(url, UserDto.class);
		
		return res;
	}
	
	@GetMapping("/api/user/auth/{username}/{password}")
	public long authenticate(@PathVariable (value = "username") String username, @PathVariable (value = "password") String password) {
		RestTemplate rt= new RestTemplate();
		String url = URL_USER_MG + "/user/auth/" + username + "/" + password;
		long res = rt.getForObject(url, long.class);
		
		return res;
	}
	
}
