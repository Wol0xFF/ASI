package springboot.controller;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;


@Controller
public class ViewController {
	
	@GetMapping("/login")
	public String login() {
		return "user/login";
	}
	
	@GetMapping("/addUser")
	public String addUser() {
		return "user/addUser";
	}
	
	@GetMapping("/home")
	public String card() {
		return "card/cardHome";
	}
	
	@GetMapping("/sell")
	public String sell() {
		return "card/cardList";
	}

	@GetMapping("/room")
	public String room() {
		return "room/roomList";
	}
}
