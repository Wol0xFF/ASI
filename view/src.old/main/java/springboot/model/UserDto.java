package springboot.model;

public class UserDto {
	private String name;
	private String surname;
	private String password;
	private long id;
	
	public UserDto() {
		super();
		this.name="";
		this.surname="";
		this.password="";
	}

	public UserDto(String name, String surname, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.password = password;
	}
	
	public UserDto(String name, String surname, String password, long id) {
		super();
		this.id=id;
		this.name = name;
		this.surname = surname;
		this.password = password;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
