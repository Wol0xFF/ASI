
    var cardData;
$(document).ready(function(){
	
	attachButtons();
	
	$.ajax("http://localhost:8080/api/cards", {
		method: "GET",
		contentType: "json",
		
	})
	.done(function(data) {
		var card;
		cardData = data;
		if(data) {
			for(var i = 0; i<data.length; i++) {
				console.log(data[i]);
				card = data[i]
				if(card) {
					addCardToList(
						card.id,
						card.imgUrl,
						card.family,
						card.imgUrl,
						card.name,
						card.description,
						card.hp,
						card.energy,
						card.attack,
						card.defence,
						card.price	
					);
				}
			}
		}
	})
	.fail(function(error) {
		console.log("error", error);
	})
	.always(attachButtons);


});




function fillCurrentCard(id,imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    //FILL THE CURRENT CARD
    $('#cardFamilyImgId')[0].src=imgUrlFamily;
    $('#cardFamilyNameId')[0].innerText=familyName;
    $('#cardImgId')[0].src=imgUrl;
    $('#cardNameId')[0].innerText=name;
    $('#cardDescriptionId')[0].innerText=description;
    $('#cardHPId')[0].innerText=hp+" HP";
    $('#cardEnergyId')[0].innerText=energy+" Energy";
    $('#cardAttackId')[0].innerText=attack+" Attack";
    $('#cardDefenceId')[0].innerText=defence+" Defence";
    $('#cardPriceId')[0].innerText=price+" $";
    $('#card_focus').setAttribute("id", function(){ return $(this.attr("id")) + id;})
};


function addCardToList(id,imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    
    content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span>"+name+" </span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td>"+price+"$</td>\
    <td>\
        <div class='ui vertical animated button' tabindex='0'>\
            <div class='hidden content'>Sell</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";
    
    $('#cardListId tr:last').after('<tr id="card_row_'+ id +'">'+content+'</tr>');
    
    
};

function attachButtons() {
	var idAttr, id;
	$("#cardListId").find(".button").each(function() {
		$(this).click(function() {
			idAttr = $(this).parents("tr").attr("id");
			id = parseInt(idAttr.substr(idAttr.indexOf("card_row_".length)));
			if(cardData) {
				for(var i = 0; i<cardData.length; i++) {
					if(cardData[i].id == id) {
						fillCurrentCard(
								cardData[i].id,
								cardData[i].imgUrl,
								cardData[i].family,
								cardData[i].imgUrl,
								cardData[i].name,
								cardData[i].description,
								cardData[i].hp,
								cardData[i].energy,
								cardData[i].attack,
								cardData[i].defence,
								cardData[i].price
							);
					}
				}
			}
		});
	});
}