
package com.cpe.springboot.card.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import com.cpe.springboot.card.model.Card;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CardRestController.class, secure = false)
public class CardRestControllerTest {
	
	@Autowired
	private CardRestController rest;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CardService cardService;

	Card mockCard = new Card(
			1,
			"yuu",
			1,
			1,
			"ttht",
			1,
			0,
			"img/gokou5.gif",
			"hyhth",
			5
	);
	
	List<Card> mockListCards = new ArrayList<>();
	
	@Test
	public void getCard() throws Exception {
		Mockito.when(
			cardService.getCard(1)
		).thenReturn(mockCard);
				

		RequestBuilder requestBuilder = 
				MockMvcRequestBuilders
					.get("/cards/1")
					.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expected = "{\"id\":1,\"family\":\"yuu\",\"attack\":1,\"defence\":1,\"description\":\"ttht\",\"energy\":1,\"hp\":0,\"imgUrl\":\"img/gokou5.gif\",\"name\":\"hyhth\",\"idUser\":5,\"sellable\":false}";


		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
		assertEquals(200, result.getResponse().getStatus());
		}
	
	
	@Test
	public void getAllCards() throws Exception {
		mockListCards.add(mockCard);
		Mockito.when(
			cardService.getAllCards()
		).thenReturn(mockListCards);
				

		RequestBuilder requestBuilder = 
				MockMvcRequestBuilders
					.get("/cards")
					.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expected = "[{\"id\":1,\"family\":\"yuu\",\"attack\":1,\"defence\":1,\"description\":\"ttht\",\"energy\":1,\"hp\":0,\"imgUrl\":\"img/gokou5.gif\",\"name\":\"hyhth\",\"idUser\":5,\"sellable\":false}]";


		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
		assertEquals(200, result.getResponse().getStatus());
		}
	
	@Test
	public void getEnergy() throws Exception {
		int energy = 50;
		Mockito.when(
				cardService.getEnergy(1)
		).thenReturn(energy);
				

		RequestBuilder requestBuilder = 
				MockMvcRequestBuilders
					.get("/cards/energy/1")
					.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println("energy : " + result.getResponse().getContentAsString());
		String expected = "50";

		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
		assertEquals(200, result.getResponse().getStatus());
	}
	
	@Test
	public void retraitEnergy() throws Exception {
		int energy = 0;
		Mockito.when(
				cardService.retraitEnergy(1)
		).thenReturn(energy);
				

		RequestBuilder requestBuilder = 
				MockMvcRequestBuilders
					.post("/cards/retraitenergy/1")
					.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println("energy : " + result.getResponse().getContentAsString());
		String expected = "0";

		assertEquals(expected, result.getResponse().getContentAsString());
		assertEquals(200, result.getResponse().getStatus());
	}
	
	@Test
	public void ajoutEnergy() throws Exception {
		int energy = 1;
		Mockito.when(
				cardService.ajoutEnergy(1)
		).thenReturn(energy);
				

		RequestBuilder requestBuilder = 
				MockMvcRequestBuilders
					.post("/cards/ajoutenergy")
					.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());
	}
}
