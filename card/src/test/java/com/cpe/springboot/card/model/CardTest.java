package com.cpe.springboot.card.model;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.card.controller.CardRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
public class CardTest {

	@Autowired
	private CardRepository cardRepository;
	
	@Test
	public void CreateCard() {
		cardRepository.save(
			new Card(
				1, 
				"family999",
				100,
				200,
				"description",
				50,
				1000,
				"imgUrl",
				"toto",
				5
			)
		);
		List<Card> cardList= cardRepository.findByFamily("family999");
		assertTrue(cardList.size() ==1);
		assertTrue(cardList.get(0).getName().equals("toto"));
	}

}
