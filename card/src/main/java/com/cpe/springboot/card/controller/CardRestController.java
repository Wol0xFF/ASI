package com.cpe.springboot.card.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.card.model.AchatTransport;
import com.cpe.springboot.card.model.Card;

@RestController
public class CardRestController {
	
	@Autowired
	private CardService CardService;
	
	@RequestMapping("/cards")
	private List<Card> getAllCourses() {
		return CardService.getAllCards();

	}
	
	@RequestMapping("/cards/{id}")
	private Card getCard(@PathVariable int id) {
		return CardService.getCard(id);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards")
	public Card addCard(@RequestBody Card card) {
		CardService.addCard(card);
		return card;
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/cards/{id}")
	public void updateCard(@RequestBody Card card,@PathVariable String id) {
		card.setId(Integer.valueOf(id));
		CardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/cards/sell")
	public void sellcard(@RequestBody Card card) {
		card.setSellable(true);
		CardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/cards/buy")
	public void buycard(@RequestBody AchatTransport achat) {
		Card card = CardService.getCard(achat.getIdCard());
		card.setIdUser(achat.getIdUser());
		CardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/cards/{id}")
	public void deleteCard(@PathVariable int id) {
		CardService.deleteCard(id);
	}
	
	@RequestMapping("/cards/family/{family}")
	private List<Card> getAllCourses(@PathVariable String family) {
		return CardService.getCardByFamily(family);

	}
	
	@RequestMapping("/cards/energy/{id}")
	private int getEnergy(@PathVariable int id) {
		return CardService.getEnergy(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards/retraitenergy/{id}")
	private int retraitEnergy(@PathVariable int id) {
		return CardService.retraitEnergy(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards/ajoutenergy")
	private void ajoutEnergy() {
		List<Card> listCard = CardService.getAllCards();
		for(Card card : listCard) {
			CardService.ajoutEnergy(card.getId());
		}
	}	
	

}
