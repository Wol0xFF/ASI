package com.cpe.springboot.card.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Card {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
	private String family;
	private Integer attack ;
	private Integer defence;
	private String description;
	private Integer energy;
	private Integer hp;
	private String imgUrl;
	private String name;
	private Integer idUser;
	private boolean sellable;
	private double price;

	public Card(Integer id, String family, Integer attack, Integer defence, String description, Integer energy,
			Integer hp, String imgUrl, String name, Integer idUser) {
		super();
		//this.id = id;
		this.family = family;
		this.attack = attack;
		this.defence = defence;
		this.description = description;
		this.energy = energy;
		this.hp = hp;
		this.imgUrl = imgUrl;
		this.name = name;
		this.idUser = idUser;
	}
	
	public Card() {
		super();
		//this.id = 0;
		this.family = "";
		this.attack = 0;
		this.defence = 0;
		this.description = "";
		this.energy = 0;
		this.hp = 0;
		this.imgUrl = "";
		this.name = "";
		this.idUser = 0;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;	
	}
	
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getDefence() {
		return defence;
	}

	public void setDefence(Integer defence) {
		this.defence = defence;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getEnergy() {
		return energy;
	}

	public void setEnergy(Integer energy) {
		this.energy = energy;
	}

	public Integer getHp() {
		return hp;
	}

	public void setHp(Integer hp) {
		this.hp = hp;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public boolean isSellable() {
		return sellable;
	}

	public void setSellable(boolean sellable) {
		this.sellable = sellable;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
}
