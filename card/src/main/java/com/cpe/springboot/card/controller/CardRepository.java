package com.cpe.springboot.card.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cpe.springboot.card.model.Card;

@Repository
public interface CardRepository extends CrudRepository<Card, Integer> {
	Card findById(int id);
	public List<Card> findByFamily(String family);

}