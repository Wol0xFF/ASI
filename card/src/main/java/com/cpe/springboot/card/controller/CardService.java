package com.cpe.springboot.card.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.model.Card;

@Service
public class CardService {

	private final int retraitEnergy = 1;
	private final int ajoutEnergy = 1;
	
	@Autowired
	private CardRepository CardRepository;

	public List<Card> getAllCards() {
		List<Card> cards = new ArrayList<>();
		CardRepository.findAll().forEach(cards::add);
		return cards;
	}

	public Card getCard(int id) {
		return CardRepository.findById(id);
	}
	
	public int getEnergy(int id) {
		int energy=-1;
		Card card = getCard(id);
		if(card != null) {
			energy = card.getEnergy();
		}
		return energy;
	}
	
	public int setEnergy(int id, int valueEnergy) {
		Card card = getCard(id);
		if(card != null) {
			card.setEnergy(valueEnergy);
		}
		return valueEnergy;
	}
	
	public int retraitEnergy(int id) {
		Card card = getCard(id);
		int energy = 0;
		if(card != null) {
			energy = card.getEnergy();
			card.setEnergy(energy - retraitEnergy);
			energy = card.getEnergy();
		}
		return energy;
	}
	
	public int ajoutEnergy(int id) {
		Card card = getCard(id);
		int energy = 0;
		if(card != null) {
			energy = card.getEnergy();
			card.setEnergy(energy + ajoutEnergy);
		}
		return energy;
	}

	public Card addCard(Card card) {
		CardRepository.save(card);
		return card;
	}

	public void updateCard(Card card) {
		CardRepository.save(card);

	}

	public void deleteCard(int id) {
		CardRepository.deleteById(id);
	}

	public List<Card> getCardByFamily(String family) {
		return CardRepository.findByFamily(family);
	}

}
