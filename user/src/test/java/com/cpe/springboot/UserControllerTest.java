package com.cpe.springboot;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import com.cpe.springboot.controller.UserController;
import com.cpe.springboot.model.UserModel;
import com.cpe.springboot.repository.UserRepository;


@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
	  @Mock
	  UserRepository userRepositoryMock;
	  @InjectMocks
	  UserController userController;
	  
	  @Test
	  public void getAllUsers() {
		  UserModel userModel = new UserModel("flavien","ahsitusavais","hamzajtmbb",8);
		  UserModel userModel2 = new UserModel("emilien","sadish","bamako",666);
		  userModel.setId(1);
		  userModel2.setId(2);
		  List<UserModel> li = new ArrayList<>();
		  li.add(userModel);
		  li.add(userModel2);
		  when(userRepositoryMock.findAll()).thenReturn(li);
		  System.out.println("Test getAllUsers");
		  assertEquals(li, userController.getAllUsers());
		  
	  }
	  @Test
	  public void getUserById() {
		  UserModel userModel = new UserModel("flavien","ahsitusavais","hamzajtmbb",8);
		  userModel.setId((long) 1);
		  when(userRepositoryMock.findById(1)).thenReturn(userModel);
		  System.out.println("Test UserByID");
		  UserModel result = userController.getUserById((long)1);
		  assertEquals(userModel, result);  
	  }
	  
	  @Test
	  public void createUser() {
		  UserModel userModel = new UserModel("flavien","ahsitusavais","hamzajtmbb",8);
		  userModel.setId((long) 1);
		  when(userRepositoryMock.save(userModel)).thenReturn(userModel);
		  System.out.println("Test createUser");
		  assertEquals(userModel, userController.createUser(userModel));
	  }
	  
	  @Test
	  public void authenticate() {
		  UserModel userModel = new UserModel("flavien","ahsitusavais","hamzajtmbb",8);
		  userModel.setId((long) 1);
		  when(userRepositoryMock.findBySurnameAndPassword("flavien", "hamzajtmbb")).thenReturn(userModel);
		  System.out.println("Test authenticateTrue");
		  assertEquals(1, userController.authenticate("flavien","hamzajtmbb"));
		  
		  System.out.println("Test authenticateFalse");
		  when(userRepositoryMock.findBySurnameAndPassword("etienne", "185")).thenReturn(null);
		  assertEquals(-1, userController.authenticate("etienne","185"));
	  }

}
