package com.cpe.springboot.model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserModel {
	
	private String name;
	private String surname;
	private String password;
	private double cash;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	public UserModel() {
		super();
		this.name="";
		this.surname="";
		this.password="";
		this.cash=100;
	}

	public UserModel(String name, String surname, String password,double cash) {
		super();
		this.name = name;
		this.surname = surname;
		this.password = password;
		this.cash=cash;
	}
	

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
