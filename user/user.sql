CREATE DATABASE  IF NOT EXISTS `user`;
USE `user`;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `id` int(11) NOT NULL,
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO user.user VALUES (1,'flavien','ahsitusavais','hamzajtmbb');
INSERT INTO user.user VALUES (2,'bertrand','laressource','saucisson');
INSERT INTO user.user VALUES (3,'hamza','mourik','chasseurcanard');
