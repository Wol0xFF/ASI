package com.cpe.springboot.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.*;


import com.cpe.springboot.model.UserModel;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long>{
	UserModel findById(long id);
	UserModel findBySurnameAndPassword(String username, String password);
}
