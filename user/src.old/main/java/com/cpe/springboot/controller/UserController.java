package com.cpe.springboot.controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.cpe.springboot.model.UserModel;
import com.cpe.springboot.repository.UserRepository;

@RestController
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/users")
	public List<UserModel> getAllUsers(){
		return this.userRepository.findAll();
	}
	@GetMapping("/users/{userId}")
	public UserModel getUserById(@PathVariable (value = "userId") long userId){
		UserModel u = this.userRepository.findById(userId);
		return u;
	}
	
	@PostMapping("/user")
	public UserModel createUser(@Valid @RequestBody UserModel user) {
		return userRepository.save(user);
	}

	@GetMapping("/user/auth/{username}/{password}")
	public long authenticate(@PathVariable (value = "username") String username, @PathVariable (value = "password") String password) {
		UserModel user = this.userRepository.findBySurnameAndPassword(username, password);
		if(user != null)
		{
			return user.getId();
		}
		
		return -1;
	}
	
	@GetMapping("/user/buy/{idUser}/{montant}")
	public boolean buy(@PathVariable (value = "idUser") long idUser, @PathVariable (value = "montant") double montant ) {
		UserModel user = this.userRepository.findById(idUser);
		if(user == null)
		{
			return false;
		}
		
		user.setCash(user.getCash()-montant);
		return (this.userRepository.save(user) != null);
	}
	
	@GetMapping("/user/sell/{idUser}/{montant}")
	public boolean sell(@PathVariable (value = "idUser") long idUser, @PathVariable (value = "montant") double montant ) {
		UserModel user = this.userRepository.findById(idUser);
		if(user == null)
		{
			return false;
		}
		
		user.setCash(user.getCash()+montant);
		return (this.userRepository.save(user) != null);
	}
	
	@GetMapping("/user/balance/{idUser}")
	public double geBalance(@PathVariable (value = "idUser") long idUser) {
		UserModel user = this.userRepository.findById(idUser);
		if(user == null)
		{
			return -1;
		}
		
		return user.getCash();
	}
}
