package com.example.demo.game.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.game.model.AchatTransport;
import com.example.demo.game.model.Game;

@RestController
public class CardRestController {
	
	@Autowired
	private CardService CardService;
	
	@RequestMapping("/cards")
	private List<Game> getAllCourses() {
		return CardService.getAllCards();

	}
	
	@RequestMapping("/cards/{id}")
	private Game getCard(@PathVariable String id) {
		return CardService.getCard(id);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards")
	public Game addCard(@RequestBody Game card) {
		CardService.addCard(card);
		return card;
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/cards/{id}")
	public void updateCard(@RequestBody Game card,@PathVariable String id) {
		card.setId(Integer.valueOf(id));
		CardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/cards/sell")
	public void sellcard(@RequestBody Game card) {
		card.setSellable(true);
		CardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/cards/buy")
	public void buycard(@RequestBody AchatTransport achat) {
		Game card = CardService.getCard(Integer.toString(achat.getIdCard()));
		card.setIdUser(achat.getIdUser());
		CardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/cards/{id}")
	public void deleteCard(@PathVariable String id) {
		CardService.deleteCard(id);
	}
	
	@RequestMapping("/cards/family/{family}")
	private List<Game> getAllCourses(@PathVariable String family) {
		return CardService.getCardByFamily(family);

	}
	
	@RequestMapping("/cards/energy/{id}")
	private int getEnergy(@PathVariable String id) {
		return CardService.getEnergy(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards/retraitenergy/{id}")
	private int retraitEnergy(@PathVariable String id) {
		return CardService.retraitEnergy(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards/ajoutenergy")
	private void ajoutEnergy() {
		List<Game> listCard = CardService.getAllCards();
		for(Game card : listCard) {
			CardService.ajoutEnergy(card.getId().toString());
		}
	}	
	

}
