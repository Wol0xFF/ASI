package com.example.demo.game.model;

public class AchatTransport {
	private int idUser;
	private int idCard;
	
	public AchatTransport(int idUser, int idCard) {
		super();
		this.idUser = idUser;
		this.idCard = idCard;
	}
	public AchatTransport() {
		super();
		this.idUser = 0;
		this.idCard = 0;
	}
	
	public int getIdUser() {
		return idUser;
	}
	
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdCard() {
		return idCard;
	}
	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}
	
	
	
}
