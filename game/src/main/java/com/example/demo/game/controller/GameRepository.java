package com.example.demo.game.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.game.model.Game;

public interface GameRepository extends CrudRepository<Game, Integer> {
	
	public List<Game> findByFamily(String family);

}