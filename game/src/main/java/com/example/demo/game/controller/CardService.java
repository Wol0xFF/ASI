package com.example.demo.game.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.game.model.Game;

@Service
public class CardService {

	private final int retraitEnergy = 1;
	private final int ajoutEnergy = 1;
	
	@Autowired
	private GameRepository GameRepository;

	public List<Game> getAllCards() {
		List<Game> games = new ArrayList<>();
		GameRepository.findAll().forEach(games::add);
		return games;
	}

	public Game getCard(String id) {
		//return GameRepository.findOne(Integer.valueOf(id));
		return new Game();
	}
	
	public int getEnergy(String id) {
		int energy=-1;
		Game game = getCard(id);
		if(game != null) {
			energy = game.getEnergy();
		}
		return energy;
	}
	
	public int setEnergy(String id, int valueEnergy) {
		Game game = getCard(id);
		if(game != null) {
			game.setEnergy(valueEnergy);
		}
		return valueEnergy;
	}
	
	public int retraitEnergy(String id) {
		Game game = getCard(id);
		int energy = 0;
		if(game != null) {
			energy = game.getEnergy();
			game.setEnergy(energy - retraitEnergy);
			energy = game.getEnergy();
		}
		return energy;
	}
	
	public int ajoutEnergy(String id) {
		Game game = getCard(id);
		int energy = 0;
		if(game != null) {
			energy = game.getEnergy();
			game.setEnergy(energy + ajoutEnergy);
		}
		return energy;
	}

	public Game addCard(Game game) {
		GameRepository.save(game);
		return game;
	}

	public void updateCard(Game game) {
		GameRepository.save(game);

	}

	public void deleteCard(String id) {
		//GameRepository.delete(Integer.valueOf(id));
	}

	public List<Game> getCardByFamily(String family) {
		return GameRepository.findByFamily(family);
	}

}
