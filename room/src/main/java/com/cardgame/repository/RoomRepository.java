package com.cardgame.repository;

import com.cardgame.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    Room findByName(String name);

    Room findById(int roomId);
}
