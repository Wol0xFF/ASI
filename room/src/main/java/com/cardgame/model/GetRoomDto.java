package com.cardgame.model;

public class GetRoomDto {
    private int id;
    private int startBet;
    private String name;
    private Integer firstPlayerId;
    private Integer firstPlayerCardId;

    public Integer getFirstPlayerId() {
        return firstPlayerId;
    }

    public void setFirstPlayerId(Integer firstPlayerId) {
        this.firstPlayerId = firstPlayerId;
    }

    public Integer getSecondPlayerId() {
        return secondPlayerId;
    }

    public void setSecondPlayerId(Integer secondPlayerId) {
        this.secondPlayerId = secondPlayerId;
    }

    private Integer secondPlayerId;
    private Integer secondPlayerCardId;
    private boolean firstPlayerReady;
    private boolean secondPlayerReady;

    public Integer getFirstPlayerCardId() {
        return firstPlayerCardId;
    }

    public void setFirstPlayerCardId(Integer firstPlayerCardId) {
        this.firstPlayerCardId = firstPlayerCardId;
    }

    public Integer getSecondPlayerCardId() {
        return secondPlayerCardId;
    }

    public void setSecondPlayerCardId(Integer secondPlayerCardId) {
        this.secondPlayerCardId = secondPlayerCardId;
    }

    public boolean isFirstPlayerReady() {
        return firstPlayerReady;
    }

    public void setFirstPlayerReady(boolean firstPlayerReady) {
        this.firstPlayerReady = firstPlayerReady;
    }

    public boolean isSecondPlayerReady() {
        return secondPlayerReady;
    }

    public void setSecondPlayerReady(boolean secondPlayerReady) {
        this.secondPlayerReady = secondPlayerReady;
    }

    public int getStartBet() {
        return startBet;
    }

    public void setStartBet(int startBet) {
        this.startBet = startBet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
