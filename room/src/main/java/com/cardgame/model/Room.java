package com.cardgame.model;

import javax.persistence.*;

@Entity
@Table(name = "room")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "start_bet")
    private int startBet;

    @Column(name = "name")
    private String name;

    @Column(name = "first_player_id")
    private Integer firstPlayerId;

    @Column(name = "first_player_card_id")
    private Integer firstPlayerCardId;

    @Column(name = "second_player_id")
    private Integer secondPlayerId;

    @Column(name = "second_player_card_id")
    private Integer secondPlayerCardId;

    public Winner getWinner() {
        return winner;
    }

    public void setWinner(Winner winner) {
        this.winner = winner;
    }

    @Enumerated(value = EnumType.STRING)
    @Column(name = "winner")
    private Winner winner =  null;

    public Integer getFirstPlayerId() {
        return firstPlayerId;
    }

    public void setFirstPlayerId(Integer firstPlayerId) {
        this.firstPlayerId = firstPlayerId;
    }

    public Integer getSecondPlayerId() {
        return secondPlayerId;
    }

    public void setSecondPlayerId(Integer secondPlayerId) {
        this.secondPlayerId = secondPlayerId;
    }

    public int getStartBet() {
        return startBet;
    }

    public void setStartBet(int startBet) {
        this.startBet = startBet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFirstPlayerReady() {
        return firstPlayerCardId != null;
    }

    public boolean isSecondPlayerReady() {
        return secondPlayerCardId != null;
    }

    public boolean areBothPlayersReady() {
        return isFirstPlayerReady() && isSecondPlayerReady();
    }

    public Integer getFirstPlayerCardId() {
        return firstPlayerCardId;
    }

    public void setFirstPlayerCardId(Integer firstPlayerCardId) {
        this.firstPlayerCardId = firstPlayerCardId;
    }

    public Integer getSecondPlayerCardId() {
        return secondPlayerCardId;
    }

    public void setSecondPlayerCardId(Integer secondPlayerCardId) {
        this.secondPlayerCardId = secondPlayerCardId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean canBeModified() {
        return !isFirstPlayerReady() && !isSecondPlayerReady();
    }

    public GetRoomDto toGetRoomDto() {
        GetRoomDto getRoomDto = new GetRoomDto();
        getRoomDto.setId(id);
        getRoomDto.setName(name);
        getRoomDto.setStartBet(startBet);
        getRoomDto.setFirstPlayerId(firstPlayerId);
        getRoomDto.setFirstPlayerCardId(firstPlayerCardId);
        getRoomDto.setSecondPlayerId(secondPlayerId);
        getRoomDto.setSecondPlayerCardId(secondPlayerCardId);
        getRoomDto.setFirstPlayerReady(isFirstPlayerReady());
        getRoomDto.setSecondPlayerReady(isSecondPlayerReady());

        return getRoomDto;
    }
}
