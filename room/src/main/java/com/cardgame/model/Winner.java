package com.cardgame.model;

public enum Winner {
    FIRST, SECOND
}
