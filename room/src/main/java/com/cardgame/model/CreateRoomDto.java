package com.cardgame.model;

public class CreateRoomDto {
    private int id;
    private int startBet;
    private String name;

    public int getStartBet() {
        return startBet;
    }

    public void setStartBet(int startBet) {
        this.startBet = startBet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
