package com.cardgame.controller;

import com.cardgame.exception.RoomException;
import com.cardgame.model.CreateRoomDto;
import com.cardgame.model.GetRoomDto;
import com.cardgame.model.Room;
import com.cardgame.model.SelectCardDto;
import com.cardgame.service.RoomService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Inject
    private RoomService roomService;

    @GetMapping
    public ResponseEntity getRooms() {
        List<GetRoomDto> rooms = this.roomService.findAll()
                .stream()
                .map(Room::toGetRoomDto)
                .collect(Collectors.toList());
        return new ResponseEntity<>(rooms, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity createOrModifyRoom(@RequestBody CreateRoomDto roomDto) {
        final Room room;
        try {
            room = this.roomService.createOrModifyRoom(roomDto);
        } catch (RoomException roomException) {
            return new ResponseEntity<>(roomException.getMessage(), HttpStatus.BAD_REQUEST);
        }
        roomDto.setId(room.getId());
        return new ResponseEntity<>(roomDto, HttpStatus.OK);
    }

    @PutMapping("/{roomId}/selectCard")
    public ResponseEntity selectCard(@PathVariable int roomId, @RequestBody SelectCardDto selectCardDto) {
        try {
            this.roomService.selectCard(roomId, selectCardDto);
         } catch (RoomException roomException) {
            return new ResponseEntity<>(roomException.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{roomId}")
    public ResponseEntity getRoom(@PathVariable int roomId) {
        final GetRoomDto getRoomDto;
        try {
            getRoomDto = this.roomService.getRoom(roomId);
        } catch (RoomException roomException) {
            return new ResponseEntity<>(roomException.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(getRoomDto, HttpStatus.OK);
    }

    @PutMapping("/{roomId}/designateWinner")
    public ResponseEntity designateWinner(@PathVariable int roomId, @RequestBody int winnerPlayerId) {
        try {
            this.roomService.designateWinner(roomId, winnerPlayerId);
        } catch (RoomException roomException) {
            return new ResponseEntity<>(roomException.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
