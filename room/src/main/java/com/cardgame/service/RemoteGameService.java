package com.cardgame.service;

import com.cardgame.config.ExternalControllerPaths;
import com.cardgame.model.GetRoomDto;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

@Service
public class RemoteGameService implements GameService {

    public void create(GetRoomDto getRoomDto) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(ExternalControllerPaths.CREATE_GAME);
            StringEntity jsonEntity = new StringEntity(new Gson().toJson(getRoomDto));
            request.setEntity(jsonEntity);
            request.setHeader("Content-type", "application/json");
            HttpResponse response = httpClient.execute(request);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }
}
