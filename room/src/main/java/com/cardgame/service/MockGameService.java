package com.cardgame.service;

import com.cardgame.model.GetRoomDto;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class MockGameService implements GameService {

    public void create(GetRoomDto getRoomDto) {
        // noop
    }
}
