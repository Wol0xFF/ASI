package com.cardgame.service;

import com.cardgame.model.GetRoomDto;

public interface GameService {

     void create(GetRoomDto getRoomDto);
}
