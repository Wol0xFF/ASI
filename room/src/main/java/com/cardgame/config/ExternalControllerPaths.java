package com.cardgame.config;

public class ExternalControllerPaths {

    public final static String GAME_SERVICE = "http://localhost:8080/";
    public final static String CREATE_GAME = GAME_SERVICE + "/game";
}
