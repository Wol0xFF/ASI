package com.cardgame.controller;

import com.cardgame.config.WebappConfiguration;
import com.cardgame.model.CreateRoomDto;
import com.cardgame.model.SelectCardDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.inject.Inject;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = WebappConfiguration.class)
public class RoomControllerTest {

    @Inject
    private RoomController roomController;

    @Test
    public void creatingAValidRoomShouldSucceed() {
        final String roomName = "testroom";
        final int startBet = 10;

        final CreateRoomDto roomDto = new CreateRoomDto();
        roomDto.setName(roomName);
        roomDto.setStartBet(startBet);
        final CreateRoomDto createdRoom = (CreateRoomDto) this.roomController.createOrModifyRoom(roomDto).getBody();

        Assert.assertNotEquals(0, createdRoom.getId());
        Assert.assertEquals(roomName, createdRoom.getName());
        Assert.assertEquals(startBet, createdRoom.getStartBet());
    }

    @Test
    public void creatingARoomWithAShortNameShouldFail() {
        CreateRoomDto roomDto = new CreateRoomDto();
        roomDto.setName("a");
        roomDto.setStartBet(10);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, this.roomController.createOrModifyRoom(roomDto).getStatusCode());
    }

    @Test
    public void updatingARoomShouldSucceed() {
        CreateRoomDto roomDto = new CreateRoomDto();
        roomDto.setName("newRoom");
        roomDto.setStartBet(10);
        ResponseEntity createOrModifyResponse = this.roomController.createOrModifyRoom(roomDto);
        Assert.assertEquals(HttpStatus.OK, createOrModifyResponse.getStatusCode());
        CreateRoomDto room = (CreateRoomDto) createOrModifyResponse.getBody();
        roomDto.setId(room.getId());
        roomDto.setName("newName");
        ResponseEntity secondCreateOrModifyResponse = this.roomController.createOrModifyRoom(roomDto);
        Assert.assertEquals(HttpStatus.OK, secondCreateOrModifyResponse.getStatusCode());
        room = (CreateRoomDto) secondCreateOrModifyResponse.getBody();
        Assert.assertEquals("newName", room.getName());

        // updating without changing the fields should work too
        Assert.assertEquals(HttpStatus.OK, this.roomController.createOrModifyRoom(roomDto).getStatusCode());
    }

    @Test
    public void selectingACardForBothPlayersShouldSucceed() {
        CreateRoomDto roomDto = new CreateRoomDto();
        roomDto.setName("newRoom173829733");
        roomDto.setStartBet(10);
        roomDto = (CreateRoomDto) this.roomController.createOrModifyRoom(roomDto).getBody();

        SelectCardDto selectCardDto = new SelectCardDto();
        selectCardDto.setCardId(12);
        selectCardDto.setPlayerId(30);
        Assert.assertEquals(HttpStatus.OK, this.roomController.selectCard(roomDto.getId(), selectCardDto).getStatusCode());
        selectCardDto.setPlayerId(35);
        Assert.assertEquals(HttpStatus.OK, this.roomController.selectCard(roomDto.getId(), selectCardDto).getStatusCode());
    }

    @Test
    public void modifyingARoomWhenAtLeastOnePlayerIsReadyShouldFail() {
        CreateRoomDto roomDto = new CreateRoomDto();
        roomDto.setName("newRoom43949304");
        roomDto.setStartBet(10);
        roomDto = (CreateRoomDto) this.roomController.createOrModifyRoom(roomDto).getBody();

        SelectCardDto selectCardDto = new SelectCardDto();
        selectCardDto.setCardId(12);
        selectCardDto.setPlayerId(20);
        this.roomController.selectCard(roomDto.getId(), selectCardDto);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, this.roomController.createOrModifyRoom(roomDto).getStatusCode());
    }

    @Test
    public void gettingARoomWhichDoesNotExistShouldFail() {
        Assert.assertEquals(HttpStatus.BAD_REQUEST, this.roomController.getRoom(-1).getStatusCode());
    }

    @Test
    public void gettingAllRoomsShouldSucceed() {
        Assert.assertEquals(HttpStatus.OK, this.roomController.getRooms().getStatusCode());
    }

    @Test
    public void designatingTheWinnerOnARoomShouldSucceed() {
        CreateRoomDto roomDto = new CreateRoomDto();
        roomDto.setName("newRoom4364a30b");
        roomDto.setStartBet(10);
        roomDto = (CreateRoomDto) this.roomController.createOrModifyRoom(roomDto).getBody();

        SelectCardDto selectCardDto = new SelectCardDto();
        selectCardDto.setCardId(12);
        selectCardDto.setPlayerId(20);
        this.roomController.selectCard(roomDto.getId(), selectCardDto);
        selectCardDto.setPlayerId(15);
        this.roomController.selectCard(roomDto.getId(), selectCardDto);

        ResponseEntity response = this.roomController.designateWinner(roomDto.getId(), 20);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void designatingTheWinnerOnARoomWherePlayersAreNotReadyShouldFail() {
        CreateRoomDto roomDto = new CreateRoomDto();
        roomDto.setName("newRoom4314a30b");
        roomDto.setStartBet(10);
        roomDto = (CreateRoomDto) this.roomController.createOrModifyRoom(roomDto).getBody();
        ResponseEntity response = this.roomController.designateWinner(roomDto.getId(), 0);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}